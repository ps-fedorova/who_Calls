"use strict";

function runTimer(sec) {
    activateButtonPopup.classList.add("activate-button__popup_hide");
    const timer = setInterval(() => {
        const seconds = sec % 60
        const minutes = sec / 60 % 60
        if (sec <= 0) {
            clearInterval(timer);
            popupInputDesc.textContent = "";
            activateButtonPopup.classList.remove("activate-button__popup_hide");
        } else {
            popupInputDesc.textContent = `Отправить код повторно (${Math.trunc(minutes)}:${seconds})`;
        }
        --sec;
    }, 1000);
}