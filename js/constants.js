"use strict";

const activateButtons = Array.from(document.querySelectorAll(".activate-button-js"));
const popup = document.querySelector(".popup");
const popupInputContainer = popup.querySelector(".popup__input-container");
const popupTitle = popup.querySelector(".popup__title");
const popupImage = popup.querySelector(".popup__image");
const popupInput = popup.querySelector(".popup__input");
const popupInputDesc = popup.querySelector(".popup__input-desc");
const popupDesc = popup.querySelector(".popup__desc");
const popupBtn = popup.querySelector(".activate-button__popup");
const popupSubtitle = popup.querySelector(".popup__subtitle");
const popupSubtitleText = popup.querySelector(".popup__subtitle_text");
const popupSubtitleNumberContainer = popup.querySelector(".popup__subtitle_number_container");
const popupSubtitleNumber = popup.querySelector(".popup__subtitle_number");
const popupSubtitleOtherNumber = popup.querySelector(".popup__subtitle-other-number");
const activateButtonPopup = popup.querySelector(".activate-button__popup");
const sec = 119;

const resetInput = () => popupInput.value = "";

let dataIndexCache;

const data = [
    {
        id: 1,
        title: "Успешно",
        img: "./images/smiling_face.png",
        alt: "smile",
        description: "Мы отправим вам подробности в SMS",
        buttonText: "Перейти в сервис"
    },
    {
        id: 2,
        title: "Запрет на подключение услуг",
        img: "./images/sad_face.png",
        alt: "sad face",
        description: "Позвоните по бесплатному номеру 0500, чтобы снять ограничения",
        buttonText: "Позвонить 0500"
    },
    {
        id: 3,
        title: "Недостаточно средств",
        img: "./images/angry_face.png",
        alt: "angry face",
        description: "Пополнить баланс можно быстро и без комиссии можно на нашем сайте или в терминале салона",
        buttonText: "Пополнить баланс"
    },
    {
        id: 4,
        title: "Что-то пошло не так",
        img: "./images/sad_face.png",
        alt: "sad face",
        description: "Попробуйте перезагрузить страницу и повторить попытку",
        buttonText: "Попробовать снова"
    },
    {
        id: 5,
        title: "Услуга уже была ранее подключена",
        img: "./images/sad_face.png",
        alt: "sad face",
        description: "Промо-код действует только для новых пользователей сервиса.",
        buttonText: "Подключить за&nbsp;7₽&nbsp;в&nbsp;день"
    },
    {
        id: 6,
        title: "Войти в сервис",
        subtitle: "Введите номер телефона и мы отправим на него SMS c кодом",
        img: "./images/man_with_book.svg",
        alt: "man with a book",
        buttonText: "Войти",
        placeholder: "+7 XXX XXX XX XX",
        maxLength: "12",
        type: "tel",
    },
    {
        id: 7,
        title: "Войти в сервис",
        subtitle: "Мы&nbsp;отправили SMS c&nbsp;кодом на&nbsp;номер",
        popupSubtitleOtherNumber: "Другой номер",
        img: "./images/man_with_book.svg",
        alt: "man with a book",
        buttonText: "Войти",
        placeholder: "XXXX",
        maxLength: "4",
        type: "numeric"
    },
];