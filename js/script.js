"use strict";

// по приходящему индексу выбираем элемент из массива data
function setDataIndex(id) {
    let dataIndex;
    data.forEach((el, idx) => {
        if (el.id === id) {
            dataIndex = idx;
        }
    })
    return dataIndex;
}

// Функция открытия и закрытия попапа
function openOrClosePopup() {
    popup.classList.toggle('popup_opened');
    if (popup.classList.contains('popup_opened')) {
        document.addEventListener('click', close);
        document.addEventListener('keydown', closeEsc);
    } else {
        document.removeEventListener('click', close);
        document.removeEventListener('keydown', closeEsc);
        resetInput();
    }
}

function setPopupData(dataIndex) {
    dataIndexCache = dataIndex;
    data[dataIndex].title && (popupTitle.textContent = data[dataIndex].title);
    data[dataIndex].img && (popupImage.src = data[dataIndex].img);
    data[dataIndex].alt && (popupImage.alt = data[dataIndex].alt);
    data[dataIndex].description && (popupDesc.textContent = data[dataIndex].description);
    data[dataIndex].buttonText && (popupBtn.innerHTML = data[dataIndex].buttonText);
    data[dataIndex].subtitle && (popupSubtitleText.innerHTML = data[dataIndex].subtitle);
    data[dataIndex].popupSubtitleOtherNumber && (popupSubtitleOtherNumber.textContent = data[dataIndex].popupSubtitleOtherNumber);
    data[dataIndex].placeholder && (popupInput.placeholder = data[dataIndex].placeholder);
    data[dataIndex].maxLength && (popupInput.maxLength = data[dataIndex].maxLength);
    data[dataIndex].type && (popupInput.type = data[dataIndex].type);

    if (popupSubtitleText.textContent) {
        popupSubtitle.classList.add("popup__text_vis");
        popupInputContainer.classList.add("popup__text_vis");
    } else {
        popupSubtitle.classList.remove("popup__text_vis");
        popupInputContainer.classList.remove("popup__text_vis");
    }

    if (popupDesc.textContent) {
        popupDesc.classList.add("popup__text_vis");
    } else {
        popupDesc.classList.remove("popup__text_vis");
    }

    if (dataIndex === 5) {
        popupSubtitleNumberContainer.classList.remove("popup__text_vis_inl");
    } else {
        popupSubtitleNumberContainer.classList.add("popup__text_vis_inl");
    }

    if (dataIndex === 5 || dataIndex === 6) {
        popupSubtitleNumber.textContent = "+7 XXX XXX XX XX";
        popupBtn.setAttribute('disabled', true);
        popupInput.addEventListener("input", () => validate(dataIndex));
    } else {
        popupInput.removeEventListener("input", () => validate(dataIndex));
    }

    if (dataIndexCache === 6) {
        activateButtonPopup.addEventListener("click", () => runTimer(sec));
        popupInputDesc.classList.remove("activate-button__popup_hide");
        popupInputDesc.textContent && activateButtonPopup.classList.add("activate-button__popup_hide");

    } else {
        activateButtonPopup.removeEventListener("click", () => runTimer(sec));
        activateButtonPopup.classList.remove("activate-button__popup_hide");
        popupInputDesc.classList.add("activate-button__popup_hide");
    }
}

function handleSuccess(res) {
    const id = res?.id;
    if (id === null || id === undefined) return;
    const dataIndex = setDataIndex(id);
    if (dataIndex === undefined) return;
    setPopupData(dataIndex);
    openOrClosePopup();
}

function handleError(err) {
    console.error(err);
}

// Закрыть по крестику и кликом по фону
function close(evt) {
    if (evt.target.classList.contains('popup__close') || evt.target.classList.contains('popup')) {
        openOrClosePopup(evt.target.closest('.popup'));
    }
}

// Закрыть кнопкой Esc 
function closeEsc(evt) {
    const popupOpened = document.querySelector('.popup_opened');
    if (evt.key === 'Escape' && popupOpened) {
        openOrClosePopup(popupOpened);
    }
}

// Слушатели
activateButtons.forEach(elem => {
    elem.addEventListener("click", () => sendRequest("GET", requestURL)
        .then(res => handleSuccess(res))
        .catch(err => handleError(err)))
});

popupSubtitleOtherNumber.addEventListener("click", () => {
    setPopupData(5);
    resetInput();
});
