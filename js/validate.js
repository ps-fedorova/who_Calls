"use strict";

function validatePhone(phone) {
    const regex = /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/;
    return regex.test(phone);
}

function validateCodeFromSms(value) {
    const regex = /[0-9]{4}/;
    return regex.test(value);
}

function validate(id) {
    popupInput.value = popupInput.value.replace(/\D/, '');

    if (id === 5 && !validatePhone(popupInput.value) || id === 6 && !validateCodeFromSms(+popupInput.value)) {
        popupBtn.setAttribute('disabled', true);
    } else {
        popupBtn.removeAttribute('disabled');
    }
}