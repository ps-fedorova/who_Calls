"use strict";

// в конец url можно подставлять разные цифры, чтобы вызвать все 7 попапов (цифры 1-7)
// это временная рыба, нужно раскидать обработку ошибок и явно будет ещё валидация.
// для этого нужен бэк

const requestURL = "https://jsonplaceholder.typicode.com/users/7";

function sendRequest(method, url, body = null) {
    const headers = {
        "Content-Type": "application/json"
    }

    return fetch(url, {
        method: method,
        body: body && JSON.stringify(body),
        headers: headers
    }).then(response => {
        if (response.ok) {
            return response.json()
        }

        return response.json();
    })
}